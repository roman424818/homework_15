#include <iostream>

void FindOddNumbers(int Limit, bool isOdd)
{
	for (int i = isOdd; i <= Limit; i+=2)
	{
		std::cout << i << "\n";
	}
}

int main()
{
	system("color F4");

	int user_value;
	bool user_odd;
	std::cout << "Enter random value: ";
	std::cin >> user_value;
	std::cout << "Enter 0 (false) or 1 (true): ";
	std::cin >> user_odd;
	FindOddNumbers(user_value, user_odd);

	return 0;
}